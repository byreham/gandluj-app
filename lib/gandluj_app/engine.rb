require "devise"
require "devise/jwt"
require "carrierwave"
require "carrierwave/orm/activerecord"
require "kaminari"
require "ransack"
require "sentry-rails"
require "sentry-ruby"
require "faraday"
require "rack/cors"
require "active_model_serializers"
require "ancestry"
require "rswag/api"
require "rswag/ui"
require_relative "file_io"

module GandlujApp
  class Engine < ::Rails::Engine
    isolate_namespace GandlujApp

    config.generators do |g|
      g.api_only!
      g.test_framework :rspec
      g.fixture_replacement :factory_bot
      g.factory_bot dir: "spec/factories"
    end

    initializer "gandluj_app.initialize" do |_app|
      ActiveSupport::Inflector.inflections(:en) do |inflect|
        inflect.acronym "IO"
        inflect.acronym "WEBP"
      end
    end
  end
end
