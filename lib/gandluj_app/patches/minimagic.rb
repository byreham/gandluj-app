module GandlujApp
  module Patches
    module MiniMagick
      def quality(percentage)
        manipulate! do |img|
          img.quality(percentage.to_s)
          img = yield(img) if block_given?
          img
        end
      end
    end
  end
end

unless CarrierWave::MiniMagick.included_modules.include?(GandlujApp::Patches::MiniMagick)
  CarrierWave::MiniMagick.include GandlujApp::Patches::MiniMagick
end
