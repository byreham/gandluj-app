module GandlujApp
  class Configuration
    attr_writer :proxy_creds

    def proxy_creds
      @proxy_creds ||= {
        host: "https://app.brestmoda.best/api/v1".freeze
      }
    end
  end
end
