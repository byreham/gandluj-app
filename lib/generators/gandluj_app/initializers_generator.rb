require "rails/generators/base"

module GandlujApp
  class InitializersGenerator < Rails::Generators::Base
    INITIALIZERS = %w[carrierwave rswag_api rswag_ui configure_host cors devise
                      kaminari ransack sentry].freeze

    desc <<-DESC.strip_heredoc
      Copy needed libraries initializers to your config/initializers folder.

      Use -i to specify which initializer you want to overwrite.
      Use -s to specify which initializer you want to skip.
      If you do no specify a controller, all initializers will be created.
      For example:
        rails generate gandluj_app:initializers -i=carrierwave
      This will copy a initializer to config/initializers/carrierwave.rb like this:
        CarrierWave.configure do |config|
          config.storage = Rails.application.credentials.fetch(:storage, :file)
          ...
        end
    DESC

    source_root File.expand_path("../templates/initializers", __dir__)

    class_option :initializers, aliases: "-i", type: :array,
                                desc: "Select specific initializers to generate (#{INITIALIZERS.join(", ")})"

    class_option :skip_initializers, aliases: "-s", type: :array,
                                     desc: "Select specific initializers to skip (#{INITIALIZERS.join(", ")})"

    def create_initializers
      initializers = options.fetch(:initializers, INITIALIZERS) - options.fetch(:skip_initializers, [])
      initializers.each do |name|
        if File.file?("config/initializers/#{name}.rb")
          update_devise_initializer if name == "devise"
          update_cors_initializer if name == "cors"
        else
          template "#{name}.rb", "config/initializers/#{name}.rb"
        end
      end
    end

    # rubocop:disable Metrics/MethodLength
    def update_devise_initializer
      inject_into_file "config/initializers/devise.rb", before: "  # ==> Controller configuration" do
        <<~STRING
          \n
            config.jwt do |jwt|
              jwt.secret = Rails.application.credentials.jwt_secret
              jwt.request_formats = { users: [nil, :json] }
              jwt.dispatch_requests = [
                ["POST", %r{^/admins/sign_in([.]json)?$}],
                ["POST", %r{^/users/sign_in([.]json)?$}]
              ]
              jwt.revocation_requests = [
                ["DELETE", %r{^/admins/sign_out([.]json)?$}],
                ["DELETE", %r{^/users/sign_out([.]json)?$}]
              ]
            end
        STRING
      end
    end

    def update_cors_initializer
      insert_into_file "config/initializers/cors.rb" do
        <<~STRING
          Rails.application.config.middleware.insert_before 0, Rack::Cors do
            allow do
              origins "localhost:5000", "localhost:4200"

              resource "*", headers: :any, methods: :any, expose: %w[Authorization], credentials: false, max_age: 600
              resource "/api/*", headers: :any, methods: %i[get], credentials: false
            end
          end
        STRING
      end
    end
    # rubocop:enable Metrics/MethodLength

    hide!
  end
end
