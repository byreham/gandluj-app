require "rails/generators/base"

module GandlujApp
  class ApiDocsGenerator < Rails::Generators::Base
    desc <<-DESC.strip_heredoc
      Generate api documents to <app_root/swagger>

      Use -p to specify where you want to store generated documentation
      If you do no specify a path, all documents will be generated at <app_root>/swagger.

      For example:
        rails generate gandluj_app:api_docs -p api_docs
      This will gnerate api documents at <app_root>/api_docs
    DESC

    source_root File.expand_path("../../../spec/dummy", __dir__)

    class_option :path, aliases: "-p", type: :string,
                        desc: "Specify a path where you want to store generated documentation"

    def copy_api_docs
      directory "swagger", options.fetch(:path, "swagger")
    end

    def add_routes
      route "mount Rswag::Ui::Engine => '/api-docs'"
      route "mount Rswag::Api::Engine => '/api-docs'"
    end

    hide!
  end
end
