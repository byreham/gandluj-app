require "rails/generators/base"

module GandlujApp
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path("../templates", __dir__)

      desc "Creates a GandlujApp initializer to your application."

      def copy_initializer
        template "gandluj_app.rb", "config/initializers/gandlu_app.rb"
      end

      def copy_migrations
        rake "gandluj_app:install:migrations"
      end

      def add_routes
        route "mount GandlujApp::Engine => '/core'"
      end

      def ruby_version
        template "ruby-version", ".ruby-version"
      end

      hide!
    end
  end
end
