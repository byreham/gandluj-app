GandlujApp.setup do |config|
  config.proxy_creds = {
    host: "https://app.brestmoda.best",
    client_id: "CHANGE_ME_TO_YOUR_CLIENT_ID",
    client_key: "CHANGE_ME_TO_YOUR_CLIENT_KEY"
  }
end
