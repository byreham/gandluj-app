Rswag::Api.configure do |c|
  c.swagger_root = Rails.root.join("/swagger")
  c.swagger_headers = { "Content-Type" => "application/json; charset=UTF-8" }
  c.swagger_filter = ->(swagger, env) { swagger["host"] = env["HTTP_HOST"] }
end
