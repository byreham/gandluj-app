Ransack.configure do |c|
  c.hide_sort_order_indicators = true
  c.postgres_fields_sort_option = :nulls_last
  c.sanitize_custom_scope_booleans = false
end
