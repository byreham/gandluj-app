require "gandluj_app/configuration"
require "gandluj_app/engine"

module GandlujApp
  def self.setup
    yield(config)
  end

  def self.config
    @config ||= GandlujApp::Configuration.new
  end
end
