require "date"

namespace :gandluj_app do
  namespace :seed do
    desc "Update model type positions, rake 'db:seed:models[14.02.2022]'"
    task :models, [:date] => [:environment] do |_t, args|
      p "Start of seeding models"
      ActiveRecord::Base.logger = Logger.new $stdout
      date = args.fetch(:date, ENV.fetch("DATE", Date.today))
      GandlujApp::ProxyModels::Create.call(date)
      p "End of seeding models"
    end
  end
end
