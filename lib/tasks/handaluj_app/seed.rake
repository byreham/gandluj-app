namespace :gandluj_app do
  desc "SYNC PROXY DATA"
  task seed: :environment do
    p "Start of seeding data"
    unless GandlujApp::Admin.where(email: "main-admin@example.com").any?
      GandlujApp::Admin.create(email: "main-admin@example.com", name: "Main Admin",
                               password: "4TCE4yasU8HZ5Lxh", password_confirmation: "4TCE4yasU8HZ5Lxh")
    end

    Rake::Task["gandluj_app:seed:models"].invoke

    p "End of seeding data"
  end
end
