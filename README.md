# GandlujApp
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem "gandluj_app"
```

And then execute:
```bash
bundle
```

Or install it yourself as:
```bash
gem install gandluj_app
```

Run the install generators:
```bash
./bin/rails generate gandluj_app:install
```

```bash
./bin/rails generate gandluj_app:initializers
```

If you want mount at your host core API documentation
```bash
./bin/rails generate gandluj_app:api_docs
```

To sync data from the Stock Core catalog:
- create an account in the Stock Core
- create an application under your account in the Stock Core
- copy credentials from there and add them to GandlujApp initializer at 'config/initializers/gandluj_app.rb'
- after that you can run the task to get related data
```bash
DATE=01.01.2022 rake gandluj_app:seed
```
This will create in your database companies, categories and products from the Stock Core

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
