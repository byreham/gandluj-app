class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.jsonb :user, defaul: {}, null: false
      t.decimal :total_price, precision: 6, scale: 2, default: 0.0, null: false
      t.string :currency, default: 'USD', null: false
      t.integer :items_count, default: 0, null: false
      t.integer :status, default: 0, null: false

      t.timestamps
    end

    create_table :orders_items do |t|
      t.references :model
      t.references :order
      t.decimal :price, precision: 6, scale: 2, default: 0.0, null: false
      t.string :currency, default: 'USD', null: false
      t.string :size, default: '', null: false
      t.text :comment
    end
  end
end
