class InitGandlujDbTables < ActiveRecord::Migration[7.0]
  def change
    create_table "companies", force: :cascade do |t|
      t.boolean "hide", default: false, null: false
      t.string "title", default: "", null: false
      t.string "slug", default: "", null: false
      t.string "logo"
      t.string :xid

      t.timestamps
    end

    create_table "users", force: :cascade do |t|
      t.string "email", default: "", null: false
      t.string "encrypted_password", default: "", null: false
      t.string "reset_password_token"
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer "sign_in_count", default: 0, null: false
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.inet "current_sign_in_ip"
      t.inet "last_sign_in_ip"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "name", default: ""
      t.integer "role", default: 0
      t.integer "companies", default: [], array: true
      t.integer "production_company_ids", default: [], array: true

      t.index ["email"], name: "index_users_on_email", unique: true
      t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    end

    create_table :models do |t|
      t.string "manufacture_number", null: false
      t.boolean "published", default: false, null: false
      t.boolean "archieved", default: false, null: false
      t.jsonb "color", default: [], null: false
      t.string "material"
      t.string "comment"
      t.string "slug", default: "", null: false
      t.string "currency", default: "USD"
      t.jsonb "model_with_other_color_ids", default: []
      t.integer "catalog_position", default: 0
      t.boolean "furniture_is_different", default: false
      t.decimal :price, null: false, default: 0.00, precision: 11, scale: 2
      t.string :available_sizes, array: true
      t.jsonb :palettes, default: [], null: false
      t.references :category
      t.references :company
      t.string :xid
      t.string :other_color_slugs, array: true

      t.timestamps
    end

    create_table "models_images", force: :cascade do |t|
      t.string "file", null: false
      t.integer "position", default: 0, null: false
      t.references :model
      t.string :xid

      t.timestamps
    end

    create_table :categories do |t|
      t.string     :name, null: false
      t.string     :slug
      t.references :parent, forein_key: { to_table: :categories }
      t.string     :xid

      t.timestamps
    end

    create_table :jwt_denylist do |t|
      t.string :jti, null: false
      t.datetime :exp, null: false
    end

    add_index :companies, :xid, unique: true
    add_index :categories, :xid, unique: true
    add_index :models, :xid, unique: true
    add_index :models_images, :xid, unique: true
    add_index :companies, :slug, unique: true
    add_index :categories, :slug, unique: true
    add_index :models, :slug, unique: true
    add_index :jwt_denylist, :jti
  end
end
