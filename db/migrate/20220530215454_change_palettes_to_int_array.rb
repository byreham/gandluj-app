class ChangePalettesToIntArray < ActiveRecord::Migration[7.0]
  def change
    add_column :models, :palette_ids, :integer, array: true, default: []
    rename_column :models, :palettes, :palettes_jsonb

    GandlujApp::Model.find_each do |model|
      model.update(palette_ids: model.palettes_jsonb.map { |p| p['id'] })
    end

    remove_column :models, :palettes_jsonb, :jsonb
  end
end
