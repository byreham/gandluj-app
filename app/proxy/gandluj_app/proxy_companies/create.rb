module GandlujApp
  module ProxyCompanies
    class Create
      attr_reader :collection

      def initialize(collection = [])
        @collection = collection
      end

      def call
        collection.map do |response|
          company = GandlujApp::ProxyCompanies::Create.build_company(response)
          company&.save
          company
        end.compact
      end

      class << self
        def call
          response = GandlujApp::ProxyClient.new.get "companies"
          collection = { companies: response.body }.with_indifferent_access[:companies]
          GandlujApp::ProxyCompanies::Create.new(collection).call
        end

        def build_company(response)
          return if response.nil?

          company = GandlujApp::Company.where(xid: response[:id]).or(
            GandlujApp::Company.where(slug: response[:slug])
          ).last || GandlujApp::Company.new
          company.assign_attributes(company_attributes_from_response(response))

          if company.logo.nil?
            company.build_logo(
              image: GandlujApp::FileIO.new(Faraday.new.get(response[:logo]).body, response[:logo].split("/").last)
            )
          end

          company
        end

        def build_and_save(response)
          return if response.nil?

          company = build_company(response)
          company.save
          company
        end

        def company_attributes_from_response(response)
          response.slice(*columns).merge(xid: response[:id])
        end

        def columns
          GandlujApp::Company.column_names.map(&:to_sym) - %i[id created_at updated_at logo]
        end
      end
    end
  end
end
