module GandlujApp
  class ProxyClient
    HOST = GandlujApp.config.proxy_creds.fetch(:host).freeze
    ID = GandlujApp.config.proxy_creds.fetch(:client_id).freeze
    KEY = GandlujApp.config.proxy_creds.fetch(:client_key).freeze

    attr_reader :conn, :auth

    delegate :get, :post, to: :conn

    def initialize(host = HOST, key = KEY)
      @conn = Faraday.new(
        host,
        headers: {
          "Content-Type" => "application/json",
          "Client-Id" => ID, "Client-Secret": key
        }
      ) do |f|
        f.response :logger
        f.response :json
      end
    end

    def call
      yield @conn
    end
  end
end
