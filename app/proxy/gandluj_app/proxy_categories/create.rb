module GandlujApp
  module ProxyCategories
    class Create
      attr_reader :collection

      def initialize(collection = [])
        @collection = collection
      end

      def call
        collection.map do |response|
          category = GandlujApp::ProxyCategories::Create.build_category(response)
          category&.save
          category
        end.compact
      end

      class << self
        def call
          response = GandlujApp::Client.new.get "categories"
          collection = { categories: response.body }.with_indifferent_access[:categories]
          GandlujApp::ProxyCategories::Create.new(collection).call
        end

        def build_category(response)
          return if response.nil?

          category = GandlujApp::Category.where(xid: response[:id]).or(
            GandlujApp::Category.where(slug: response[:slug])
          ).last || GandlujApp::Category.new
          category.assign_attributes(category_attributes_from_response(response))

          category.parent = GandlujApp::ProxyCategories::Create.build_and_save(response[:parent])
          category
        end

        def build_and_save(response)
          return if response.nil?

          category = build_category(response)
          category.save
          category
        end

        def category_attributes_from_response(response)
          response.slice(*columns).merge(xid: response[:id])
        end

        def columns
          GandlujApp::Category.column_names.map(&:to_sym) - %i[id created_at updated_at]
        end
      end
    end
  end
end
