module GandlujApp
  module ProxyModels
    class Create
      attr_reader :collection

      def initialize(collection = [])
        @collection = collection
      end

      def call
        collection.map do |response|
          model = GandlujApp::ProxyModels::Create.build_model(response)

          unless model.persisted?
            company = GandlujApp::ProxyCompanies::Create.build_and_save(response[:company])
            category = GandlujApp::ProxyCategories::Create.build_and_save(response[:category])
            model.assign_attributes(company_id: company&.id, category_id: category&.id)
          end

          log_error(model) unless model.save
          model
        end
      end

      def log_error(record)
        logger.error(
          <<~HEREDOC
            **************************************************
            MODEL with XID='#{record.xid}' not saved!
              ERRORS: #{record.errors.full_messages.join(", ")}
            ATTRIBUTES:
              #{record.inspect}
            **************************************************
          HEREDOC
        )
      end

      def logger
        @logger ||= self.class.logger
      end

      class << self
        # rubocop:disable Metrics/MethodLength
        def call(*dates)
          client = GandlujApp::ProxyClient.new
          from_date, to_date = dates.map { |date| date.is_a?(String) ? DateTime.parse(date) : date }
          i = 0
          next_page = true

          while next_page
            i += 1
            logger.info "GET page #{i}"

            response = client.get "models", {
              search: { created_at_gteq: from_date, created_at_lteq: to_date },
              page: i
            }
            next_page = response.body.present?
            collection = { models: response.body }.with_indifferent_access[:models]
            GandlujApp::ProxyModels::Create.new(collection).call
          end
        end
        # rubocop:enable Metrics/MethodLength

        def build_model(response)
          return if response.nil?

          model = GandlujApp::Model.where(xid: response[:id]).or(
            GandlujApp::Model.where(slug: response[:slug])
          ).last || GandlujApp::Model.new(archieved: false, published: true)
          model.assign_attributes(model_attributes_from_response(response))

          build_images(response, model) unless model.persisted?

          model
        end

        def build_images(response, model)
          response[:images].each do |image|
            next if model.images.where(xid: image[:id]).any?

            model.images.build(build_image_attributes(image))
          end
        end

        def build_image_attributes(image)
          {
            xid: image[:id], position: image[:position],
            file: GandlujApp::FileIO.new(Faraday.new.get(image[:url]).body, image[:url].split("/").last)
          }
        end

        def model_attributes_from_response(response)
          response.slice(*columns).merge(xid: response[:id])
                  .merge(palette_ids: (response[:palettes].presence || []).map { |p| p[:id] })
        end

        def columns
          GandlujApp::Model.column_names.map(&:to_sym) - %i[id created_at updated_at palettes]
        end

        def logger
          @logger ||= ActiveSupport::TaggedLogging.new(
            ActiveSupport::Logger.new("log/proxy_models.log")
          )
        end
      end
    end
  end
end
