require "singleton"

module GandlujApp
  class Constants
    include Singleton

    PALETTES = [
      { id: 1, name: "dark blue", name_ru: "синий", rgb: "#00f" },
      { id: 2, name: "black", name_ru: "черный", rgb: "#000" },
      { id: 3, name: "red", name_ru: "красный", rgb: "#f00" },
      { id: 4, name: "turquoise", name_ru: "бирюзовый", rgb: "#30D5C8" },
      { id: 5, name: "grey", name_ru: "серый", rgb: "#666" },
      { id: 6, name: "white", name_ru: "белый", rgb: "#fff" },
      { id: 7, name: "beige", name_ru: "бежевый", rgb: "#f5f5dc" },
      { id: 8, name: "pink", name_ru: "розовый", rgb: "#ffc0cb" },
      { id: 9, name: "green", name_ru: "зеленый", rgb: "#008000" },
      { id: 10, name: "yellow", name_ru: "желтый", rgb: "#ff0" },
      { id: 11, name: "light blue", name_ru: "голубой", rgb: "#add8e6" },
      { id: 12, name: "burgundy", name_ru: "бордовый", rgb: "#a52a2a" },
      { id: 13, name: "orange", name_ru: "оранжевый", rgb: "#ff8830" },
      { id: 14, name: "purple", name_ru: "фиолетовый", rgb: "#9f00ff" },
      { id: 15, name: "multi", name_ru: "мульти", rgb: "conic-gradient(indigo, purple, red, orange, yellow, green, aqua, blue)" }
    ].freeze

    SIZES = %w[
      38 40 42 44 46 48 50 52 54 56 58 60 62 64 66 68 70 72 74 76 78 80 82
    ].freeze
  end
end
