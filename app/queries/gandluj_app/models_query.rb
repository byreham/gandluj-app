module GandlujApp
  class ModelsQuery
    attr_reader :search_params

    def initialize(search_params, **additional_params)
      @search_params = search_params.merge(additional_params)
    end

    def call(page = 1, per_page = 36)
      query
      order
      result
      paginate(page, per_page)
    end

    private

    def result
      @result ||= query.result
                       .includes(:company)
                       .includes(:category)
                       .preload(:images)
    end

    def paginate(page, per_page)
      result.page(page).per(per_page)
    end

    def query
      @query ||= GandlujApp::Model.ransack(search_params)
    end

    def order
      query.sorts = search_params.fetch(:sorts, ["created_at desc"]) if query.sorts.empty?
    end
  end
end
