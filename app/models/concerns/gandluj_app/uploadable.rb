# frozen_string_literal: true

# Define helper methods to get the URL of downloaded file versions.
module GandlujApp
  module Uploadable
    extend ActiveSupport::Concern

    module ClassMethods
      def mount_uploader(column_name, uploader_class = nil, options = {}, &)
        define_uploader_helpers(column_name, uploader_class)
        # call CarrierWave::ActiveRecord#mount_uploader with passed args
        super(column_name, uploader_class, options, &)
      end

      private

      def define_uploader_helpers(column_name, uploader_class)
        uploader_class.versions.each do |version_name, _|
          define_method "#{column_name}_#{version_name}_url" do
            public_send(column_name).public_send(version_name)&.url
          end
        end
      end
    end
  end
end
