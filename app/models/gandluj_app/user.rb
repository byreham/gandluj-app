module GandlujApp
  class User < ApplicationRecord
    self.table_name = "users"

    devise :database_authenticatable, :registerable, :recoverable, :validatable,
           :jwt_authenticatable, jwt_revocation_strategy: GandlujApp::JwtDenylist

    enum role: { user: 0, admin: 9 }
  end
end
