module GandlujApp
  module Orders
    class Item < ApplicationRecord
      self.table_name = "orders_items"

      belongs_to :order, inverse_of: :items, class_name: "GandlujApp::Order", counter_cache: true
      belongs_to :model, class_name: "GandlujApp::Model"

      validates_presence_of :price, :currency, :size
    end
  end
end
