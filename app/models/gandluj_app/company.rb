require "carrierwave"

module GandlujApp
  class Company < GandlujApp::ApplicationRecord
    self.table_name = "companies"

    mount_uploader :logo, GandlujApp::LogoUploader

    has_many :models, class_name: "GandlujApp::Model", dependent: :delete_all

    validates :title, presence: true
    validates :slug, uniqueness: true

    scope :visible, -> { where(hide: false) }
  end
end
