module GandlujApp
  class Order < GandlujApp::ApplicationRecord
    self.table_name = "orders"

    STATUSES = {
      initial: 0,
      processing: 1,
      processed: 2
    }.freeze

    has_many :items, inverse_of: :order, class_name: "GandlujApp::Orders::Item", foreign_key: :order_id, dependent: :delete_all

    accepts_nested_attributes_for :items

    enum status: STATUSES
  end
end
