module GandlujApp
  class Admin < GandlujApp::ApplicationRecord
    self.table_name = "admins"

    devise :database_authenticatable, :registerable, :recoverable, :validatable,
           :jwt_authenticatable, jwt_revocation_strategy: GandlujApp::JwtDenylist
  end
end
