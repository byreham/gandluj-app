module GandlujApp
  class JwtDenylist < GandlujApp::ApplicationRecord
    include Devise::JWT::RevocationStrategies::Denylist

    self.table_name = "jwt_denylist"
  end
end
