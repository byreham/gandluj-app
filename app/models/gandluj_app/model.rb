module GandlujApp
  class Model < GandlujApp::ApplicationRecord
    self.table_name = "models"

    RUB_RATE = 83
    MARGIN = 20

    belongs_to :company, class_name: "GandlujApp::Company"
    belongs_to :category, inverse_of: :models, class_name: "GandlujApp::Category"
    has_many :images, -> { order(position: :asc) }, class_name: "GandlujApp::Models::Image"

    attr_reader :palettes_jsonb

    serialize :palettes_jsonb

    validates :xid, uniqueness: true
    validates :slug, uniqueness: true

    scope :published, -> { where(published: true) }
    scope :visible_models, -> { joins(:company).where(companies: { hide: false }) }
    # acts_as_list scope: :model

    scope :active_size_id_in, lambda { |sizes|
      ids = sizes.is_a?(Array) ? sizes.join(",") : sizes
      where("available_sizes && ?", "{#{ids}}")
    }

    scope :active_sizes_in, ->(sizes) { active_size_id_in(sizes) }

    scope :palette_ids_array_in, lambda { |ids|
      values = ids.is_a?(Array) ? ids.join(",") : ids
      where("palette_ids && ?", "{#{values}}")
    }

    scope :friendly_where_slugs, ->(slugs) { where(slug: slugs) }
    scope :friendly_slug_eq, ->(slug) { friendly_where_slugs(slug).order("models.id ASC").limit(1) }

    scope :price_range, lambda { |*args|
      values, _currency = args
      range = Range.new(*values.split("..").map { |value| (value.to_d / RUB_RATE) - MARGIN })
      where(price: range)
    }

    ransack_alias :search_text, :manufacture_number_or_company_title_or_category_name_or_size
    ransack_alias :text, :manufacture_number_or_company_title_or_category_name_or_size

    def self.ransackable_scopes(_auth_object = nil)
      %i[created_at_the_month orders_count coefficient_sort visits_count palette_ids_array_in fabric_type_ids_array_in
         model_style_ids_array_in friendly_where_slugs friendly_slug_eq price_range active_size_id_in]
    end

    ransacker :size do
      Arel.sql("array_to_string(available_sizes, ',')")
    end

    def price_converted
      price * RUB_RATE
    end

    def full_price_converted
      full_price * RUB_RATE
    end

    def full_price
      price + MARGIN
    end

    def root_category
      category.root
    end

    def title
      category&.name_single.presence || [manufacture_number, color].compact.join(" ")
    end

    def title_without_company
      [category&.name, manufacture_number, color].compact.join(", ")
    end

    def breadcrumbs_title
      [manufacture_number, color].compact.join(" ")
    end

    def category_name
      category&.name_single || category&.name
    end

    def palette_rgb
      palette.try(:[], :rgb)
    end

    def palettes
      @palettes ||= GandlujApp::Constants::PALETTES.select { |palette| palette_ids&.include? palette[:id] }
    end

    # rubocop:disable Metrics/AbcSize
    def palette
      colors = palettes.each_with_object({ name: color, rgb: [] }) do |item, acc|
        item = item.with_indifferent_access
        acc[:name] = acc[:name].present? ? acc[:name] + " + #{item[:name_ru]}" : item[:name_ru]
        acc[:rgb] << item[:rgb]
      end

      return { name: colors[:name], rgb: colors[:rgb].join } if palettes.size == 1 || colors[:rgb].size.zero?

      color_part = 100 / colors[:rgb].size
      gradient = colors[:rgb].map.with_index { |rgb, index| "#{rgb} #{color_part * index}%" }.join(",")
      { name: colors[:name], rgb: "linear-gradient(#{gradient})" }
    end
    # rubocop:enable Metrics/AbcSize
  end
end
