module GandlujApp
  class Category < GandlujApp::ApplicationRecord
    self.table_name = "categories"

    SINGUALARIZED_SET = {
      "Костюмы" => "Костюм женский",
      "Брючные" => "Костюм женский",
      "Юбочные" => "Костюм женский",
      "Двойка" => "Костюм женский",
      "Тройка" => "Костюм женский",
      "Платья" => "Платье женское",
      "Блузки" => "Блузка женская",
      "Куртки" => "Куртка женская",
      "Пальто" => "Пальто женское",
      "Плащи" => "Плащ женский",
      "Юбки" => "Юбка женская",
      "Брюки" => "Брюки женские",
      "Жилеты" => "Жилет женский",
      "Шорты" => "Шорты женские",
      "Комбинезоны" => "Комбинизон женский",
      "Кофты" => "Кофта женская",
      "Спорт" => "Спорт-одежда женская"
    }.freeze

    has_ancestry orphan_strategy: :adopt

    has_many :models, inverse_of: :category, class_name: "GandlujApp::Model", dependent: :nullify

    validates :name, presence: true
    validates :xid, uniqueness: true
    validates :slug, uniqueness: true

    def name_single
      # super.presence ||
      SINGUALARIZED_SET[name].presence || name.try(:singularize)
    end
  end
end
