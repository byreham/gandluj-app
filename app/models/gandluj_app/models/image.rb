module GandlujApp
  module Models
    class Image < ApplicationRecord
      include GandlujApp::Uploadable
      self.table_name = "models_images"

      mount_uploader :file, GandlujApp::ModelsImageUploader

      belongs_to :model, class_name: "GandlujApp::Model"

      validates :xid, uniqueness: true
    end
  end
end
