module GandlujApp
  module Orders
    class Create < GandlujApp::BaseService
      attr_reader :params, :order, :order_items

      def initialize(params = {})
        @params = params
        @order = Order.new(user: params.fetch(:user, {}))
        @order.items = build_items
        super()
      end

      def call
        order.save ? success : failed
      end

      def errors
        order.errors.full_messages
      end

      def build_items
        params.fetch(:order_items, []).map do |item_params|
          Orders::Item.new(item_params.slice(:comment)).tap do |item|
            assign_attributes(item, item_params)
          end
        end.compact
      end

      private

      def assign_attributes(item, item_params)
        item.model = Model.find_by(slug: item_params[:slug])
        return if item.model.nil?

        item.size = item_params[:size_id]
        item.price = item.model&.price.presence || 0
        order.total_price += item.price
      end
    end
  end
end
