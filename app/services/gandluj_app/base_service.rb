module GandlujApp
  class BaseService
    SUCCESS = "SUCCESS".freeze
    FAILED = "FAILED".freeze

    attr_reader :errors

    def self.call(*args, &)
      new(*args, &).call
    end

    def initialize
      yield if block_given?
    end

    def call
      raise NotImplemented, class_name
    end

    def success?
      result == SUCCESS
    end

    def failed?
      result == FAILED
    end

    private

    def result
      @__result
    end

    def success
      @__result = SUCCESS
      self
    end

    def failed
      @__result = FAILED
      self
    end
  end
end
