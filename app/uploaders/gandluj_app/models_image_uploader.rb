require_relative "concerns/webp"

module GandlujApp
  class ModelsImageUploader < GandlujApp::BaseUploader
    process resize_to_fit: [1333, 2000]

    version :webp do
      include GandlujApp::Concerns::WEBP
    end

    version :webp_medium do
      include GandlujApp::Concerns::WEBP
      process resize_to_fit: [500, 750]
    end

    version :webp_small do
      include GandlujApp::Concerns::WEBP
      process resize_to_fit: [320, 480]
    end

    version :webp_thumb do
      include GandlujApp::Concerns::WEBP
      process resize_to_fit: [200, 300]
    end

    version :medium do
      process resize_to_fit: [500, 750]
    end

    version :small, from_version: :medium do
      process resize_to_fit: [320, 480]
    end

    version :thumb, from_version: :medium do
      process resize_to_fit: [200, 300]
    end

    def store_dir
      "uploads/gandluj_app/models/image/file/#{model.id}"
    end

    def extension_white_list
      %w[jpg jpeg gif png webp]
    end

    def content_type_allowlist
      %r{image/}
    end

    def size_range
      0..(10.megabytes)
    end

    def alt_for_model_image
      title_for_model_image
    end

    def title_for_category
      object = model.model
      [
        object.category&.name_single,
        object.company&.title,
        object.manufacture_number,
        object.color
      ].compact.join(" ")
    end

    protected

    # Required to actually force Amazon S3 to treat it like an image
    def set_content_type_to_webp
      file.instance_variable_set(:@content_type, "image/webp")
    end
  end
end
