module GandlujApp
  module Concerns
    module WEBP
      extend ActiveSupport::Concern

      included do
        include InstanceMethods

        process convert: "webp"
        process :set_content_type_to_webp
      end

      module InstanceMethods
        def full_filename(*args)
          name = super.sub("webp_", "")
          extension = File.extname(name)
          name.sub(extension, ".webp").to_s
        end
      end
    end
  end
end
