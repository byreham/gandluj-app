module GandlujApp
  class LogoUploader < GandlujApp::BaseUploader
    version :thumb do
      process resize_to_fit: [100, 100]
    end

    def content_type_allowlist
      %r{image/}
    end

    def extension_white_list
      %w[jpg jpeg gif png]
    end
  end
end
