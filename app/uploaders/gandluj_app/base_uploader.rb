module GandlujApp
  class BaseUploader < CarrierWave::Uploader::Base
    include CarrierWave::MiniMagick

    def store_dir
      "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    end

    def filename
      original_filename || (path || "").split("/").last
    end

    def url(*args)
      return default_url(args) if blank?

      super
    end

    def default_url(*_args)
      "#{asset_host}/images/fallback/#{[version_name, "no-image.png"].compact.join("_")}"
    end
  end
end
