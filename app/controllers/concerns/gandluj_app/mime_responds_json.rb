module GandlujApp
  module MimeRespondsJson
    extend ActiveSupport::Concern

    included do
      include ActionController::MimeResponds
      respond_to :json
    end
  end
end
