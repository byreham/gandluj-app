module GandlujApp
  module V1
    class ModelsController < GandlujApp::V1::ApplicationController
      def show
        render json: model
      end

      private

      def model
        @model ||= GandlujApp::Model.find_by!(slug: params[:slug])
      end
    end
  end
end
