module GandlujApp
  module V1
    class CatalogSettingsController < GandlujApp::V1::ApplicationController
      def index
        @categories = GandlujApp::Category.arrange_serializable do |parent, children|
          GandlujApp::V1::CategorySerializer.new(parent, children:)
        end.as_json(include: "**")
        @palettes = GandlujApp::Constants::PALETTES
        @sizes = GandlujApp::Constants::SIZES.map { |size| { name: size, id: size } }
        render json: {
          categories: @categories, palettes: @palettes, sizes: @sizes
        }
      end
    end
  end
end
