module GandlujApp
  module V1
    class CatalogController < GandlujApp::V1::ApplicationController
      def index
        @models = GandlujApp::ModelsQuery.new(search_params).call(params[:page], params[:per_page])
        render json: {
          count: @models.total_count,
          models: ActiveModel::Serializer::CollectionSerializer.new(@models, serializer: GandlujApp::V1::ModelSerializer)
        }
      end

      def show
        category_ids = Category.find_by(slug: params[:categories].split("/").last).subtree_ids
        @models = GandlujApp::ModelsQuery.new(search_params, category_id_in: category_ids).call(params[:page], params[:per_page])
        render json: {
          count: @models.total_count,
          models: ActiveModel::Serializer::CollectionSerializer.new(@models, serializer: GandlujApp::V1::ModelSerializer)
        }
      end

      private

      def search_params
        params.fetch(:search, {}).permit(:manufacture_number_cont, :company_id_eq, :sorts, :created_at_the_month, :search_text_i_cont,
                                         :text, :price_range, :published_eq, :archieved_eq,
                                         :active_size_id_in, :palette_ids_array_in, slug_in: [])
      end
    end
  end
end
