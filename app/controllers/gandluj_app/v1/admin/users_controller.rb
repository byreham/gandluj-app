module GandlujApp
  module V1
    module Admin
      class UsersController < GandlujApp::V1::Admin::ApplicationController
        before_action :user, only: %i[show update destroy]

        # GET /users
        def index
          @users = GandlujApp::User.all

          render json: @users
        end

        # GET /users/1
        def show
          render json: @user
        end

        # POST /users
        def create
          @user = GandlujApp::User.new(user_params)

          if @user.save
            render json: @user, status: :created, location: @user
          else
            render json: @user.errors, status: :unprocessable_entity
          end
        end

        # PATCH/PUT /users/1
        def update
          if @user.update(user_params)
            render json: @user
          else
            render json: @user.errors, status: :unprocessable_entity
          end
        end

        # DELETE /users/1
        def destroy
          @user.destroy
        end

        private

        # Use callbacks to share common setup or constraints between actions.
        def user
          @user ||= GandlujApp::User.find(params[:id])
        end

        # Only allow a list of trusted parameters through.
        def user_params
          params.require(:user).permit(:id, :name, :email, role: [], companies: [])
        end
      end
    end
  end
end
