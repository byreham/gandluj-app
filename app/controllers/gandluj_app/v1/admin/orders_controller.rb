module GandlujApp
  module V1
    module Admin
      class OrdersController < GandlujApp::V1::Admin::ApplicationController
        before_action :order, only: %i[show update destroy]

        def index
          query = GandlujApp::Order.ransack(search_params)
          query.sorts = search_params.fetch(:sorts, ["created_at desc"]) if query.sorts.empty?

          @orders = query.result
                         .includes(items: [{ model: %i[images category] }])
                         .page(params[:page])
                         .per(params[:per_page])
          render json: @orders, include: { items: { model: :preview } }
        end

        def show
          render json: @order
        end

        def create
          @order = GandlujApp::Order.new(order_params)

          if @order.save
            render json: @order, status: :created, location: @order
          else
            render json: @order.errors, status: :unprocessable_entity
          end
        end

        def update
          if @order.update(order_params)
            render json: @order
          else
            render json: @order.errors, status: :unprocessable_entity
          end
        end

        def destroy
          @subscriber.destroy
        end

        private

        def order
          @order = GandlujApp::Order.find(params[:id])
        end

        def order_params
          params.require(:order).permit(:id)
        end

        def search_params
          params.fetch(:q, {}).permit(:status_eq)
        end
      end
    end
  end
end
