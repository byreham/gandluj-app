module GandlujApp
  module V1
    module Admin
      class CompaniesController < GandlujApp::V1::Admin::ApplicationController
        before_action :company, only: %i[show update destroy]

        def index
          @companies = GandlujApp::Company.where(hide: false)
          render json: @companies
        end

        def show
          render json: @company
        end

        def create
          @company = GandlujApp::Company.new(company_params)

          if @company.save
            render json: @company, status: :created, location: @company
          else
            render json: @company.errors, status: :unprocessable_entity
          end
        end

        def update
          if @company.update(company_params)
            render json: @company
          else
            render json: @company.errors, status: :unprocessable_entity
          end
        end

        def destroy
          @subscriber.destroy
        end

        private

        def company
          @company = GandlujApp::Company.find(params[:id])
        end

        def company_params
          params.require(:company).permit(:id, :title, :hide)
        end
      end
    end
  end
end
