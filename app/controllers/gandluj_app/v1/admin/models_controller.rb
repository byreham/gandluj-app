module GandlujApp
  module V1
    module Admin
      class ModelsController < GandlujApp::V1::Admin::ApplicationController
        before_action :model, only: %i[show update destroy]

        def index
          @models = GandlujApp::ModelsQuery.new(search_params).call(params[:page], params[:per_page])
          render json: @models
        end

        def show
          render json: @model
        end

        def create
          @model = Model.new(model_params)

          if @model.save
            render json: @model, status: :created, location: @model
          else
            render json: @model.errors, status: :unprocessable_entity
          end
        end

        def update
          if @model.update(model_params)
            render json: @model
          else
            render json: @model.errors, status: :unprocessable_entity
          end
        end

        def destroy
          @subscriber.destroy
        end

        private

        def model
          @model = GandlujApp::Model.find(params[:id])
        end

        def model_params
          params.require(:model).permit(:id)
        end

        def search_params
          params.fetch(:q, {}).permit(:manufacture_number_cont, :company_id_eq, :sorts, :created_at_the_month,
                                      :published_eq, :archieved_eq)
        end
      end
    end
  end
end
