module GandlujApp
  module V1
    module Admin
      class ApplicationController < GandlujApp::ApplicationController
        respond_to :json

        before_action :authenticate_admin!
        before_action do
          self.namespace_for_serializer = V1::Admin
        end
      end
    end
  end
end
