module GandlujApp
  module V1
    class ApplicationController < GandlujApp::ApplicationController
      respond_to :json

      before_action do
        self.namespace_for_serializer = V1
      end
    end
  end
end
