module GandlujApp
  module V1
    class OrdersController < GandlujApp::V1::ApplicationController
      def create
        GandlujApp::Orders::Create.call(order_params).tap do |service|
          if service.success?
            render json: service.order, status: :created
          else
            render json: service.errors, status: :unprocessable_entity
          end
        end
      end

      private

      def order_params
        params.require(:order).permit(user_params, order_items_params, :delivery, :comment)
      end

      def user_params
        { user: %i[name first_name last_name middle_name phone_number country region city address post_code] }
      end

      def order_items_params
        { order_items: %i[slug size_id price currency comment] }
      end
    end
  end
end
