module GandlujApp
  module V1
    class SessionsController < Devise::SessionsController
      include GandlujApp::MimeRespondsJson

      def create
        super { |resource| @resource = resource }
      end

      def respond_to_on_destroy
        head :ok
      end
    end
  end
end
