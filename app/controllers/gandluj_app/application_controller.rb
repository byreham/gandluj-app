module GandlujApp
  class ApplicationController < ::ApplicationController
    include ActionController::RequestForgeryProtection
    include GandlujApp::MimeRespondsJson

    before_action :verify_authenticity_token
    skip_before_action :verify_authenticity_token
  end
end
