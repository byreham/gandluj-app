module GandlujApp
  module V1
    module Admin
      class OrderSerializer < ActiveModel::Serializer
        attributes :id, :user, :status, :total_price, :currency, :items_count, :created_at

        has_many :items, serializer: GandlujApp::V1::Admin::Orders::ItemSerializer
      end
    end
  end
end
