module GandlujApp
  module V1
    module Admin
      module Orders
        class ItemSerializer < ActiveModel::Serializer
          attributes :id, :size, :price, :currency, :comment

          belongs_to :model, serializer: GandlujApp::V1::Admin::Orders::ModelSerializer
        end
      end
    end
  end
end
