module GandlujApp
  module V1
    module Admin
      module Orders
        class ModelSerializer < ActiveModel::Serializer
          attributes :id, :manufacture_number, :color, :slug,
                     :other_color_slugs, :available_sizes,
                     :price, :price_converted, :currency

          has_one :images, key: :preview do
            object.images.first.file.webp_thumb.url
          end

          def currency
            "USD"
          end

          def price
            object.price.to_s
          end

          def price_converted
            object.price_converted.to_s
          end
        end
      end
    end
  end
end
