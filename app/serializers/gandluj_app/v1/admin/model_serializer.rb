module GandlujApp
  module V1
    module Admin
      class ModelSerializer < ActiveModel::Serializer
        attributes :id, :manufacture_number, :color, :material, :comment, :slug,
                   :other_color_slugs, :model_with_other_color_ids,
                   :catalog_position, :furniture_is_different, :palette,
                   :available_sizes, :palettes, :published, :archieved,
                   :price, :price_converted, :currency, :created_at, :updated_at

        has_one :company, serializer: GandlujApp::V1::Admin::CompanySerializer
        has_one :category, serializer: GandlujApp::V1::Admin::CategorySerializer
        has_many :images, serializer: GandlujApp::V1::Admin::Models::ImageSerializer
        has_one :images, key: :preview, serializer: GandlujApp::V1::Admin::Models::ImageSerializer do
          object.images.first
        end

        def price
          object.price.to_s
        end

        def price_converted
          object.price_converted.to_s
        end
      end
    end
  end
end
