module GandlujApp
  module V1
    module Admin
      module Models
        class ImageSerializer < ActiveModel::Serializer
          attributes :id, :position, :versions

          def versions
            versions = object.file.versions
            return if versions.blank?

            versions.transform_values(&:url)
          end

          def url
            object.file_url
          end

          def thumb_url
            object.file&.thumb&.url
          end
        end
      end
    end
  end
end
