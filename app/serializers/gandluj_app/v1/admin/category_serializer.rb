module GandlujApp
  module V1
    module Admin
      class CategorySerializer < ActiveModel::Serializer
        attributes :id, :name, :slug
      end
    end
  end
end
