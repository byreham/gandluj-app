module GandlujApp
  module V1
    class CompanySerializer < ActiveModel::Serializer
      attributes :id, :title, :hide, :slug, :logo

      def logo
        object.logo_url
      end
    end
  end
end
