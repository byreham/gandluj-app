module GandlujApp
  module V1
    class CategorySerializer < ActiveModel::Serializer
      attributes :id, :name, :slug

      has_many :children, serializer: GandlujApp::V1::CategorySerializer
    end
  end
end
