module GandlujApp
  module V1
    class ModelSerializer < ActiveModel::Serializer
      attributes :id, :manufacture_number, :published, :archieved, :color, :title,
                 :preview, :material, :comment, :palettes,
                 :images, :created_at, :slug, :model_with_other_color_ids,
                 :catalog_position, :furniture_is_different

      attributes :domain, :price,
                 :preview, :zoom_preview, :model_type_slug, :root_model_type_slug, :model_with_other_color_ids,
                 :model_type_name, :available_colors, :price_converted, :currency,
                 :webp_preview, :webp_zoom_preview, :available_items, :palette

      has_one :company, serializer: GandlujApp::V1::CompanySerializer

      def available_items
        object.available_sizes.map do |size|
          { id: size, size:, active: true, size_id: size }
        end
      end

      def domain
        Rails.application.secrets.sync_domain
      end

      def currency
        "RUB"
      end

      def price
        # object.full_price.format
        # PriceConverter.round(object.full_price, "USD").format
        object.full_price
      end

      def price_converted
        # PriceConverter.round(object.price_converted, "RUB").format
        object.full_price_converted
      end

      def preview
        object.images.first&.file_webp_thumb_url
      end

      def zoom_preview
        object.images.first&.file_webp_url
      end

      def webp_preview
        object.images.first&.file_webp_medium_url
      end

      def webp_zoom_preview
        object.images.first&.file_webp_url
      end

      def images
        object.images.map do |img|
          { id: img.id, url: img.file_url, slug: img.xid,
            webp_url: img.file_webp_url, webp_medium_url: img.file_webp_medium_url }
        end
      end

      def model_type_slug
        object.category.try(:slug)
      end

      def root_model_type_slug
        object.root_category.try(:slug)
      end

      def model_with_other_colors
        GandlujApp::Model
          .where(slug: object.model_with_other_color_ids.concat(object.other_color_slugs))
          .includes(:images)
          .select(:slug, :palette_ids, :color)
      end

      def model_with_other_color_ids
        model_with_other_colors.map(&:slug)
      end

      def model_type_name
        object.category_name
      end

      def available_colors
        available_colors = [{ slug: object.slug, preview:, color: object.palette_rgb, active: true }]
        model_with_other_colors.each do |model|
          available_colors.push(
            slug: model.slug, preview: model.images.first&.file_webp_thumb_url,
            color: model.palette_rgb, active: false
          )
        end
        available_colors
      end
    end
  end
end
