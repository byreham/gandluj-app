GandlujApp::Engine.routes.draw do
  namespace 'v1', defaults: { format: :json } do
    namespace 'admin' do
      resources :users, only: %i[index create update]
      resources :companies, only: %i[index create update]
      resources :models, only: %i[index create update]
      resources :orders, only: %i[index create update]
    end

    devise_for :admin, class_name: 'GandlujApp::Admin', singular: :admin, controllers: { sessions: "gandluj_app/v1/admin/sessions" }
    devise_for :users, class_name: 'GandlujApp::User', singular: :user, controllers: { sessions: "gandluj_app/v1/sessions" }

    resources :catalog_settings, only: %i[index]
    resources :catalog, only: %i[index] do
      collection do
        match '/:categories', via: :get, action: 'show', requirements: { categories: /.*/ },
                              constraints: { categories: %r{([a-zA-Z0-9_-]+/?){0,}} }
      end
    end
    resources :models, only: %i[show], param: :slug
    resources :orders, only: %i[create]
  end
end
