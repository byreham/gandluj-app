$LOAD_PATH.push File.expand_path("lib", __dir__)
require_relative "lib/gandluj_app/version"

Gem::Specification.new do |spec|
  spec.name        = "gandluj_app"
  spec.version     = GandlujApp::VERSION
  spec.authors     = ["Byreham"]
  spec.email       = ["byreham@gmail.com"]
  spec.homepage    = "https://gitlab.com/byreham/gandluj-app"
  spec.summary     = "Summary of GandlujApp."
  spec.description = "Description of GandlujApp."
  spec.license     = "MIT"

  spec.required_ruby_version = "~> 3.1"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = "https://gitlab.com/byreham/gandluj-app"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/byreham/gandluj-app"
  spec.metadata["changelog_uri"] = "https://gitlab.com/byreham/gandluj-app/-/blob/main/CHANGELOG.md"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "active_model_serializers", ">= 0.10.0"
  spec.add_dependency "ancestry", ">= 4.1.0"
  spec.add_dependency "carrierwave", ">= 2.2.2"
  spec.add_dependency "devise", ">= 4.8.1"
  spec.add_dependency "devise-jwt", ">= 0.9.0"
  spec.add_dependency "faraday", ">= 1.10.0"
  spec.add_dependency "faraday_middleware", ">= 1.2.0"
  spec.add_dependency "fog-aws", ">= 3.13.0"
  spec.add_dependency "kaminari", ">= 1.2.2"
  spec.add_dependency "pg", ">= 1.2"
  spec.add_dependency "pry", ">= 0.14.1"
  spec.add_dependency "rack-cors", ">= 1.1.1"
  spec.add_dependency "rails", ">= 7.0.2.3"
  spec.add_dependency "ransack", ">= 3.0.1"
  spec.add_dependency "rswag-api", ">= 2.5.1"
  spec.add_dependency "rswag-ui", ">= 2.5.1"

  # error cacthing and reporting
  spec.add_dependency "sentry-rails", ">= 5.2.1"
  spec.add_dependency "sentry-ruby", ">= 5.2.1"

  spec.add_development_dependency "annotate", ">= 3.2.0"
  spec.add_development_dependency "factory_bot_rails", ">= 6.2.0"
  spec.add_development_dependency "pry", ">= 0.14.1"
  spec.add_development_dependency "rspec-rails", ">= 5.1.2"
  spec.add_development_dependency "rswag-specs", ">= 2.5.1"
  spec.add_development_dependency "rubocop", ">= 1.21"

  spec.metadata["rubygems_mfa_required"] = "true"
end
