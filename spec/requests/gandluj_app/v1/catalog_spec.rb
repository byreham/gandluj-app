require 'swagger_helper'

RSpec.describe '/catalog', type: :request do
  path "/catalog" do
    get "Product list" do
      tags "catalog"
      consumes "application/json"
      parameter name: :search, in: :params, schema: {
        type: :object,
        properties: {
          manufacture_number_cont: { type: :integer },
          company_id_eq: { type: :integer },
          sorts: { type: :string },
          search_text_i_cont: { type: :string },
          text: { type: :string },
          price_range: { type: :string },
          active_size_id_in: { type: :array },
          slug_in: { type: :array },
          palette_ids_array_in: { type: :array },
        },
      }
      response "200", "Retrieves models" do
        example 'application/json', :catalog, [{
          id: 732,
          manufacture_number: "ИВ-1790",
          published: true,
          archieved: false,
          color: "коралл",
          title: "Повседневные платья",
          preview: "http://localhost:3000/uploads/gandluj_app/models/image/file/1793/thumb_1790....webp",
          material: "полиэстр 100%",
          comment:
          "Платье женское прямого свободного силуэта. Горловина округлая, обработана бейкой. Застежка до линии талии на петли-пуговицы.\nПо переду  на уровне  талии выполнен фигурный подрез, от которого выполнены подрезы к боковым швам. По подрезам распределена небольшая сборка. По низу переда фигурный асимметричный подрез. Верх переда и спинки обработан кокеткой без плечевого шва. \nСпинка со средним швом.\nРукав спущен, по низу с патой, крепящейся на пуговицу.\nПо всем швам выполнена отделка на двухигольной машине. \n\nФурнитура может отличаться от заявленной на фото. \n\nДлина платья - 110 см, ширина по линии груди - 64/66/68, ширина по линии бедер - 72/74/76, длина рукава - 42 см, ширина рукава -22/23/24.",
          palettes: [{ id: 3, rgb: "#f00", name: "red", name_ru: "красный" }, { id: 8, rgb: "#ffc0cb", name: "pink", name_ru: "розовый" }],
          images:
          [{
            id: 1793,
            url: "http://localhost:3000/uploads/gandluj_app/models/image/file/1793/1790....jpg",
            slug: "03e82382-f9d1-4970-a874-eb39ffaa9eff",
            webp_url: "http://localhost:3000/uploads/gandluj_app/models/image/file/1793/1790....webp",
            webp_medium_url: "http://localhost:3000/uploads/gandluj_app/models/image/file/1793/medium_1790....webp"
          },
          {
            id: 1794,
            url: "http://localhost:3000/uploads/gandluj_app/models/image/file/1794/1790.jpg",
            slug: "03e5e9a2-5144-4341-a557-8eaa1553a585",
            webp_url: "http://localhost:3000/uploads/gandluj_app/models/image/file/1794/1790.webp",
            webp_medium_url: "http://localhost:3000/uploads/gandluj_app/models/image/file/1794/medium_1790.webp"
          }],
          created_at: "Tue, 03 May 2022 10:05:09.076656000 UTC +00:00",
          slug: "ivelta-iv-1790-korall",
          model_with_other_color_ids: [],
          catalog_position: 131166,
          furniture_is_different: true,
          domain: nil,
          price: 30,
          zoom_preview: "http://localhost:3000/uploads/gandluj_app/models/image/file/1793/1790....webp",
          model_type_slug: "povsednevnye-platya",
          root_model_type_slug: "zhenskaya-odezhda",
          model_type_name: "Повседневные платья",
          available_colors:
          [{
            slug: "ivelta-iv-1790-korall", preview: "http://localhost:3000/uploads/gandluj_app/models/image/file/1793/thumb_1790....webp",
            color: "linear-gradient(#f00 0%,#ffc0cb 50%)", active: true
          }],
          price_converted: 2490,
          currency: "RUB",
          webp_preview: "http://localhost:3000/uploads/gandluj_app/models/image/file/1793/medium_1790....webp",
          webp_zoom_preview: "http://localhost:3000/uploads/gandluj_app/models/image/file/1793/1790....webp",
          available_items: [
            { id: "60", size: "60", active: true, size_id: "60" },
            { id: "58", size: "58", active: true, size_id: "58" },
            { id: "56", size: "56", active: true, size_id: "56" }
          ],
          palette: { name: "коралл + красный + розовый",  rgb: "linear-gradient(#f00 0%,#ffc0cb 50%)" },
          company: { id: 19, title: "Ivelta plus", hide: false, slug: "de3e4c5d-6ed1-4efc-8b97-2f3f610ec693", logo: "http://localhost:3000/images/fallback/no-image.png" }
        }]
        run_test!
      end
    end
  end
end
