Rails.application.routes.draw do
  mount GandlujApp::Engine => "/core"
  mount Rswag::Api::Engine => "/docs"
  mount Rswag::Ui::Engine => "/docs"
end
