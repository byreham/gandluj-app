Sentry.init do |config|
  config.enabled_environments = %w[production]
  config.dsn = Rails.application.credentials.fetch(:sentry_dsn, "")
  config.async = ->(event, hint) { Sentry::SendEventJob.perform_later(event, hint) }
  config.excluded_exceptions += ["ActionController::RoutingError", "ActiveRecord::RecordNotFound"]
  config.inspect_exception_causes_for_exclusion = true
  config.logger = Logger.new($stdout).tap { |logger| logger.level = Logger::ERROR }
  config.breadcrumbs_logger = %i[active_support_logger http_logger]
  config.backtrace_cleanup_callback = lambda do |backtrace|
    Rails.backtrace_cleaner.clean(backtrace)
  end
  config.traces_sample_rate = 0.5 if Rails.env.production?
  config.send_client_reports = false
end

Rails.application.configure do
  config.middleware.use Sentry::Rack::CaptureExceptions
end
