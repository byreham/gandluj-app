Rails.application.configure do
  host_uri = URI(Rails.application.credentials.fetch(:host, "http://localhost:3000")).freeze
  config.asset_host = host_uri.to_s
  config.action_controller.asset_host = config.asset_host
  # config.action_mailer.asset_host = config.asset_host
  config.default_url_options = { host: host_uri.host, port: host_uri.port }
  config.action_controller.default_url_options = config.default_url_options
  # config.action_mailer.default_url_options = config.default_url_options
  Rails.application.routes.default_url_options = config.default_url_options
end
