CarrierWave.configure do |config|
  config.storage = Rails.application.credentials.fetch(:storage, :file)

  if Rails.application.credentials.fog.present?
    fog_config = Rails.application.credentials.fog

    config.fog_credentials = fog_config[:credentials]
    config.fog_directory = fog_config[:directory]
    config.fog_public = fog_config[:public]
    config.asset_host = fog_config[:asset_host]
  end

  config.fog_attributes = { cache_control: "public, max-age=#{365.days.to_i}" }
end
