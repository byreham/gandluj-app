GandlujApp.setup do |config|
  config.proxy_creds = Rails.application.credentials.fetch(:proxy_creds, {})
end
