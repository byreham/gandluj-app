# Be sure to restart your server when you modify this file.

# Avoid CORS issues when API is called from the frontend app.
# Handle Cross-Origin Resource Sharing (CORS) in order to accept cross-origin AJAX requests.

# Read more: https://github.com/cyu/rack-cors

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins "localhost:5000", "localhost:4200"

    resource "/core/v1/", headers: :any, methods: :any, credentials: false
    resource "/core/v1/admin/*", headers: :any, methods: :any, expose: %w[Authorization], credentials: false, max_age: 600
  end
end
