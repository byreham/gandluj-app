# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_05_03_121746) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name", null: false
    t.string "slug"
    t.bigint "parent_id"
    t.string "xid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.index ["ancestry"], name: "index_categories_on_ancestry"
    t.index ["parent_id"], name: "index_categories_on_parent_id"
    t.index ["slug"], name: "index_categories_on_slug", unique: true
    t.index ["xid"], name: "index_categories_on_xid", unique: true
  end

  create_table "companies", force: :cascade do |t|
    t.boolean "hide", default: false, null: false
    t.string "title", default: "", null: false
    t.string "slug", default: "", null: false
    t.string "logo"
    t.string "xid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_companies_on_slug", unique: true
    t.index ["xid"], name: "index_companies_on_xid", unique: true
  end

  create_table "jwt_denylist", force: :cascade do |t|
    t.string "jti", null: false
    t.datetime "exp", null: false
    t.index ["jti"], name: "index_jwt_denylist_on_jti"
  end

  create_table "models", force: :cascade do |t|
    t.string "manufacture_number", null: false
    t.boolean "published", default: false, null: false
    t.boolean "archieved", default: false, null: false
    t.jsonb "color", default: [], null: false
    t.string "material"
    t.string "comment"
    t.string "slug", default: "", null: false
    t.string "currency", default: "USD"
    t.jsonb "model_with_other_color_ids", default: []
    t.integer "catalog_position", default: 0
    t.boolean "furniture_is_different", default: false
    t.decimal "price", precision: 11, scale: 2, default: "0.0", null: false
    t.string "available_sizes", array: true
    t.jsonb "palettes", default: [], null: false
    t.bigint "category_id"
    t.bigint "company_id"
    t.string "xid"
    t.string "other_color_slugs", array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_models_on_category_id"
    t.index ["company_id"], name: "index_models_on_company_id"
    t.index ["slug"], name: "index_models_on_slug", unique: true
    t.index ["xid"], name: "index_models_on_xid", unique: true
  end

  create_table "models_images", force: :cascade do |t|
    t.string "file", null: false
    t.integer "position", default: 0, null: false
    t.bigint "model_id"
    t.string "xid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["model_id"], name: "index_models_images_on_model_id"
    t.index ["xid"], name: "index_models_images_on_xid", unique: true
  end

  create_table "orders", force: :cascade do |t|
    t.jsonb "user", null: false
    t.decimal "total_price", precision: 6, scale: 2, default: "0.0", null: false
    t.string "currency", default: "USD", null: false
    t.integer "items_count", default: 0, null: false
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders_items", force: :cascade do |t|
    t.bigint "model_id"
    t.bigint "order_id"
    t.decimal "price", precision: 6, scale: 2, default: "0.0", null: false
    t.string "currency", default: "USD", null: false
    t.string "size", default: "", null: false
    t.text "comment"
    t.index ["model_id"], name: "index_orders_items_on_model_id"
    t.index ["order_id"], name: "index_orders_items_on_order_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", default: ""
    t.integer "role", default: 0
    t.integer "companies", default: [], array: true
    t.integer "production_company_ids", default: [], array: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
